all:
	g++ -std=c++0x -O3 -Wunused-result -o rwg.exe server.cpp client.cpp shell.cpp command.cpp pipe.cpp unit.cpp


clean:
	rm rwg.exe
