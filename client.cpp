#include "client.h"

std::vector<Client> Client::clients;
int Client::indice[MAX_CLIENT_NUMBER] = {0};

Client::Client(int sock_fd, std::string ip, int port) {
    this->sock_fd = sock_fd;
    this->name = "(no name)";
    this->ip = ip;
    this->port = port;

    this->ip = "CGILAB";
    this->port = 511;

    for (int i=0; i<MAX_CLIENT_NUMBER; i++) {
        if (!Client::indice[i]) {
            this->index = i;
            Client::indice[i] = 1;
            break;
        }
    }

    shell = new Shell();
    setEnvironmentVariable("PATH","bin:.");
}

Client::~Client() {}

int Client::getSockFd() {
    return sock_fd;
}

std::string Client::getName() {
    return name;
}
void Client::setName(std::string name) {
    this->name = name;
}

std::string Client::getIp() {
    return ip;
}

int Client::getPort() {
    return port;
}

int Client::getIndex() {
    return index;
}

void Client::setEnvironmentVariable(std::string name, std::string value) {
    std::map<std::string, std::string>::iterator iter = environment_variables.find(name);
    if(iter != environment_variables.end()) {
        iter->second = value;
    } else {
        environment_variables.insert(std::pair<std::string, std::string>(name, value));
    }
}

std::map<std::string, std::string> Client::getEnvironmentVariable() {
    std::map<std::string, std::string> copy_env_vars(environment_variables);
    return copy_env_vars;
}

void Client::showWelcomeMessage() {
    std::string message = "****************************************\n"
                          "** Welcome to the information server. **\n"
                          "****************************************\n";

    write(sock_fd, message.c_str(), message.size());
}

void Client::prompt() {
    std::string prompt = "% ";

    write(sock_fd, prompt.c_str(), prompt.size());
}

int Client::exec(std::string input) {
    Shell::state_code = 1;
    std::vector<Command> commands = shell->parseCommand(input, sock_fd);
    shell->execCommand(commands ,sock_fd);
    return Shell::state_code;
}