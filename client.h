#ifndef RAS_CLIENT_H_
#define RAS_CLIENT_H_

#include <string>
#include <vector>
#include <map>
#include <unistd.h>
#include "shell.h"
#include "command.h"

#define MAX_CLIENT_NUMBER 30

class Client {

public:
    Client(int sock_fd, std::string ip, int port);

    ~Client();

    static std::vector<Client> clients;
    static int indice[MAX_CLIENT_NUMBER];

    int getSockFd();

    std::string getName();
    void setName(std::string name);

    std::string getIp();

    int getPort();

    int getIndex();

    void setEnvironmentVariable(std::string name, std::string value);

    std::map<std::string, std::string> getEnvironmentVariable();

    void showWelcomeMessage();

    void prompt();

    int exec(std::string input);

private:
    int sock_fd;
    std::string name;
    std::string ip;
    int port;

    int index;

    std::map<std::string, std::string> environment_variables;

    Shell *shell;
};

#endif