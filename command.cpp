#include "command.h"
#include "unit.h"
#include "shell.h"
#include "client.h"

#include <unistd.h>
#include <sstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

std::map< std::string, int> Command::command_type_map = {
    {"exit", INTER_COMMAND_TYPE},
    {"setenv", INTER_COMMAND_TYPE},
    {"printenv", INTER_COMMAND_TYPE},
    {"who", INTER_COMMAND_TYPE},
    {"tell", INTER_COMMAND_TYPE},
    {"yell", INTER_COMMAND_TYPE},
    {"name", INTER_COMMAND_TYPE},
    {"ls", EXTER_COMMAND_TYPE},
    {"cat", EXTER_COMMAND_TYPE},
    {"removetag", EXTER_COMMAND_TYPE},
    {"removetag0", EXTER_COMMAND_TYPE},
    {"number", EXTER_COMMAND_TYPE},
    {"noop", EXTER_COMMAND_TYPE}
};

std::map< std::string, Command::handler > Command::inter_command_handler_map = {
    {"exit", Command::exitShell},
    {"setenv", Command::setEnv},
    {"printenv", Command::printEnv},
    {"who", Command::who},
    {"tell", Command::tell},
    {"yell", Command::yell},
    {"name", Command::named}
};

// ============================== Public ==============================
Command::Command() {

}

Command::Command(std::string name, std::string arguments) {
    this->name = unit::deleteStringFrontSpace(name);
    this->pipe_number = 0;
    this->out_fd = -1;
    this->in_fd = -1;
    parseArguments(arguments);
}

Command::~Command() {

}
void Command::setName(std::string name) {
    this->name = name;
}
std::string Command::getName() {
    return name;
}

std::vector< std::string > Command::getArguments() {
    return arguments;
}

void Command::setArgumnets(std::string arguments) {
    this->arguments.clear();
    parseArguments(arguments);
}

int Command::getPipeNumber() {
    return pipe_number;
}

void Command::setPipeNumber(int pipe_number) {
    this->pipe_number = pipe_number;
}

std::string Command::getFilename() {
    return filename;
}

void Command::setFilename(std::string filename) {
    this->filename = filename;
}

int Command::getOutFd() {
    return out_fd;
}
void Command::setOutFd(int out_fd) {
    this->out_fd = out_fd;
}

int Command::getInFd() {
    return in_fd;
}
void Command::setInFd(int out_fd) {
    this->in_fd = in_fd;
}

void Command::exec(int fd) {
    int type = getCommandType();
    std::string unknown_msg = "Unknown command: [" + name + "].\n";

    if (type == UNKNOW_COMMAND_TYPE) {
        write(fd, unknown_msg.c_str(), unknown_msg.size());
    } else if (type == INTER_COMMAND_TYPE) {
        (*inter_command_handler_map.find(name)->second)(arguments, fd);

    } else {
        const char *argv[arguments.size() + 2];
        argv[0] = name.c_str();

        for (int i=0; i<arguments.size(); i++) {
            argv[i+1] = arguments[i].c_str();
        }

        argv[arguments.size() + 1] = NULL;

        execvp(argv[0], (char **) argv);
        write(fd, unknown_msg.c_str(), unknown_msg.size());
    }
}

int Command::getCommandType() {
    std::map< std::string, int>::iterator it = command_type_map.find(name);
    if (it != command_type_map.end()) {
        return it->second;
    }

    return UNKNOW_COMMAND_TYPE;
}

// ============================== Private ==============================
void Command::exitShell(std::vector<std::string> arguments, int sock_fd) {
    Shell::state_code = -1;
}

void Command::setEnv(std::vector<std::string> arguments, int sock_fd) {
    if (arguments.size() < 2) {
        std::cout << "The argument amount of setenv should exactly euqal 2." << std::endl;
    }
    setenv(arguments[0].c_str(), arguments[1].c_str(), 1);
    for (int i=0; i<Client::clients.size(); i++) {
        if (Client::clients[i].getSockFd() == sock_fd) {
            Client::clients[i].setEnvironmentVariable(arguments[0], arguments[1]);
        }
    }
}

void Command::printEnv(std::vector<std::string> arguments, int sock_fd) {
    for (std::string arg : arguments) {
        const char *result = getenv(arg.c_str());
        if (result) {
            std::string msg = arg + "=" + result + "\n";
            write(sock_fd, msg.c_str(), msg.size());
        }
    }
}

void Command::who(std::vector<std::string> arguments, int sock_fd) {
    std::string info = "<ID>\t<nickname>\t<IP/port>\t<indicate me>\n";
    write(sock_fd, info.c_str(), info.size());
    for (int idx=0; idx < MAX_CLIENT_NUMBER; idx ++) {
        if (Client::indice[idx]) {
            for (int i=0; i<Client::clients.size(); i++) {
                Client c = Client::clients[i];
                if (idx == c.getIndex()) {
                    info = unit::to_string(c.getIndex()+1) + "\t" +
                                        c.getName() + "\t" +
                                        c.getIp() + "/" + unit::to_string(c.getPort());
                    if (c.getSockFd() == sock_fd) {
                        info += "\t<-me";
                    }
                    info += "\n";
                    write(sock_fd, info.c_str(), info.size());
                }
            }
        }
    }
}

void Command::tell(std::vector<std::string> arguments, int sock_fd) {
    int fd = atoi(arguments[0].c_str());
    std::string msg;
    std::string split = "";
    for (int i=1; i<arguments.size(); i++) {
        msg += split + arguments[i];
        split = " ";
    }

    std::string teller;
    for (int i=0; i<Client::clients.size(); i++) {
        if (Client::clients[i].getSockFd() == sock_fd) {
            teller = Client::clients[i].getName();
        }
    }

    bool found = false;
    std::string info = "*** " + teller + " told you ***: " + msg + "\n";
    for (int i=0; i<Client::clients.size(); i++) {
        if ((Client::clients[i].getIndex()+1) == fd) {
            write(Client::clients[i].getSockFd(), info.c_str(), info.size());
            found = true;
        }
    }

    info = "*** Error: user #" + unit::to_string(fd) + " does not exist yet. ***\n";
    if (!found) {
        write(sock_fd, info.c_str(), info.size());
    }

}

void Command::yell(std::vector<std::string> arguments, int sock_fd) {
    std::string msg;
    std::string split = "";
    for (int i=0; i<arguments.size(); i++) {
        msg += split + arguments[i];
        split = " ";
    }

    std::string teller;
    for (int i=0; i<Client::clients.size(); i++) {
        if (Client::clients[i].getSockFd() == sock_fd) {
            teller = Client::clients[i].getName();
        }
    }

    std::string info = "*** " + teller + " yelled ***: " + msg + "\n";
    for (int i=0; i<Client::clients.size(); i++) {
        write(Client::clients[i].getSockFd(), info.c_str(), info.size());
    }
}

void Command::named(std::vector<std::string> arguments, int sock_fd) {
    std::string n = arguments[0];
    std::string info;
    bool err = false;
    for (int i=0; i<Client::clients.size(); i++) {
        if (Client::clients[i].getName() == n) {
            info = "*** User '" + n +"' already exists. ***\n";
            write(sock_fd, info.c_str(), info.size());
            err = true;
        }
    }

    if (!err) {
        std::string ip_port;
        for (int i=0; i<Client::clients.size(); i++) {
            if (Client::clients[i].getSockFd() == sock_fd) {
                Client::clients[i].setName(n);
                ip_port = Client::clients[i].getIp() + "/" + unit::to_string(Client::clients[i].getPort());
            }
        }
        info = "*** User from " + ip_port + " is named '" + n + "'. ***\n";
        for (int i=0; i<Client::clients.size(); i++) {
            write(Client::clients[i].getSockFd(), info.c_str(), info.size());
        }
    }
}

void Command::parseArguments(std::string argument_str) {
    argument_str = unit::deleteStringFrontSpace(argument_str);

    if (this->name == "yell") {
        this->arguments.push_back(argument_str);
    } else if (this->name == "tell") {
        int index = argument_str.find(" ");
        std::string id = argument_str.substr(0, index);
        std::string msg = argument_str.substr(index+1);
        this->arguments.push_back(id);
        this->arguments.push_back(msg);
    } else {
        if (argument_str.size() > 1) {

            std::istringstream iss(argument_str);
            while (!iss.eof()) {
                std::string arg;
                iss >> arg;
                if (arg.compare("\0") != 0) {
                    if (arg.at(0) == FILE_REDIRECTION_OUT_SYMBOL) {
                        if (arg.size() == 1) {
                            iss >> arg;
                            for (int i=0; i<arg.size(); i++) {
                                filename += arg.at(i);
                            }
                        } else {
                            std::string target_process;
                            for (int i=1; i<arg.size(); i++) {
                                target_process += arg.at(i);
                            }
                            out_fd = atoi(target_process.c_str());
                        }
                    } else if (arg.at(0) == FILE_REDIRECTION_IN_SYMBOL) {
                        std::string target_process;
                        for (int i=1; i<arg.size(); i++) {
                            target_process += arg.at(i);
                        }
                        in_fd = atoi(target_process.c_str());
                    } else {
                        this->arguments.push_back(arg);
                    }
                }
            }
        }
    }
}