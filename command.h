#ifndef RAS_COMMAND_H_
#define RAS_COMMAND_H_

#include <string>
#include <vector>
#include <map>

#define UNKNOW_COMMAND_TYPE -1
#define INTER_COMMAND_TYPE 0
#define EXTER_COMMAND_TYPE 1

#define FILE_REDIRECTION_OUT_SYMBOL '>'
#define FILE_REDIRECTION_IN_SYMBOL '<'

class Command {
public:
    Command();
    Command(std::string name, std::string arguments);
    ~Command();

    void setName(std::string name);
    std::string getName();

    std::vector< std::string > getArguments();
    void setArgumnets(std::string arguments);

    int getPipeNumber();
    void setPipeNumber(int pipe_number);
    
    std::string getFilename();
    void setFilename(std::string filename);

    int getOutFd();
    void setOutFd(int out_fd);

    int getInFd();
    void setInFd(int out_fd);

    int getCommandType();

    void exec(int fd);

    std::string entire_command;
    
private:
    typedef void (*handler)(std::vector<std::string>, int);
    static std::map< std::string, int> command_type_map;
    static std::map< std::string, handler> inter_command_handler_map;

    static void exitShell(std::vector<std::string> arguments, int sock_fd);
    static void setEnv(std::vector<std::string> arguments, int sock_fd);
    static void printEnv(std::vector<std::string> arguments, int sock_fd);

    static void who(std::vector<std::string> arguments, int sock_fd);
    static void tell(std::vector<std::string> arguments, int sock_fd);
    static void yell(std::vector<std::string> arguments, int sock_fd);
    static void named(std::vector<std::string> arguments, int sock_fd);

    std::string name;
    std::vector< std::string > arguments;
    std::string filename;

    int out_fd;
    int in_fd;

    int pipe_number;

    void parseArguments(std::string arguments);

};
#endif