#include "pipe.h"

// ============================== Public ==============================

Pipe::Pipe(int fd_read, int fd_write, int count) {
    this->fd_read = fd_read;
    this->fd_write = fd_write;
    this->count = count;
}
Pipe::~Pipe() {

}

int Pipe::getFdRead() {
    return fd_read;
}
int Pipe::getFdWrite() {
    return fd_write;
}
int Pipe::getCount() {
    return count;
}

void Pipe::passCount() {
    count -= 1;
}

void Pipe::addCount() {
    count += 1;
}

// ============================== Private ==============================