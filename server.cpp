#include "client.h"
#include "shell.h"
#include "unit.h"

#include <string>
#include <cstring>
#include <vector>
#include <map>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFFER_SIZE 10000

fd_set sock_fds;
int main(int argc, char const *argv[])
{
    int port = 7001;
    std::string shell_directory(".");

    for (int i = 1; i < argc; i++) {
        std::string para(argv[i]);
        if ((para.find("--port=") == 0) || (para.find("-p=") == 0)) {
            port = atoi(para.substr(para.find("=") + 1).c_str());
        } else if ((para.find("--shell_directory=") == 0) || (para.find("-sd=") == 0)) {
            shell_directory = para.substr(para.find("=") + 1);
        }
    }

    if (chdir(shell_directory.c_str()) == -1) {
        fprintf(stderr, "The directory \"%s\" does not exist.\n", shell_directory.c_str());
        exit(1);
    }

    int listen_fd;
    struct sockaddr_in listen_address;
    memset((char*)&listen_address, 0, sizeof(listen_address));

    listen_address.sin_family = AF_INET;
    listen_address.sin_addr.s_addr = htonl(INADDR_ANY);
    listen_address.sin_port = htons(port);

    if ((listen_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        fprintf(stderr, "Fail to create socket.\n");
    }

    if(bind(listen_fd, (struct sockaddr *) &listen_address, sizeof(listen_address)) == -1) {
        fprintf(stderr, "Fail to bind socket.\n");
        exit(1);
    }

    if(listen(listen_fd, 0) == -1) {
        fprintf(stderr, "Fail to listen socket.\n");
        exit(1);
    }

    while (true) {
        FD_ZERO(&sock_fds);
        FD_SET(listen_fd, &sock_fds);

        for (int i=0; i<Client::clients.size(); i++) {
            Client client = Client::clients[i];
            FD_SET(client.getSockFd(), &sock_fds);
        }

        int select_state = select(1024, &sock_fds, NULL, NULL, NULL);
        if (select_state < 0) {
            fprintf(stderr, "Execute select() Failed\n");
        }

        if(FD_ISSET(listen_fd, &sock_fds)){

            int client_sock_fd;
            int client_len = 0;
            struct sockaddr_in client_addr;

            client_len = sizeof(client_addr);
            client_sock_fd = accept(listen_fd, (struct sockaddr*) &client_addr, (socklen_t*)&client_len);

            Client client(client_sock_fd, inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
            Client::clients.push_back(client);

            client.showWelcomeMessage();

            std::string info = "*** User '" + client.getName() + "' entered from " 
                                + client.getIp() + "/" + unit::to_string(client.getPort()) +". ***\n";
            for (int i=0; i<Client::clients.size(); i++) {
                write(Client::clients[i].getSockFd(), info.c_str(), info.size());
            }
            client.prompt();
        }
        int erase_lise[Client::clients.size()] = {0};
        for (int i=0; i<Client::clients.size(); i++) {
            Client client = Client::clients[i];
            std::map<std::string, std::string> env_vars = client.getEnvironmentVariable();
            for (auto &iter : env_vars) {
                setenv((iter.first).c_str(), (iter.second).c_str(), 1);
            }

            if(FD_ISSET(client.getSockFd(), &sock_fds)) {
                char buff[BUFFER_SIZE];
                int size = recv(client.getSockFd(), buff, sizeof(buff), 0);
                std::string input;

                for (int j=0; j<size; j++) {
                    if ((buff[j] == '\0') || (buff[j] == '\r') || (buff[j] == '\n')) {
                        break;
                    } else {
                        input += buff[j];
                    }
                }
                input += '\0';

                int state_code = client.exec(input);

                if (state_code < 0) {
                    std::string info = "*** User '" + client.getName() + "' left. ***\n";
                    for (int i=0; i<Client::clients.size(); i++) {
                        write(Client::clients[i].getSockFd(), info.c_str(), info.size());
                    }
                    close(client.getSockFd());
                    // printf("Before\n");
                    // for(auto elem : Shell::process_pipes)
                    // {
                    //     printf("%d\n", elem.first);
                    // }
                    if (Shell::process_pipes.find(client.getIndex()+1) != Shell::process_pipes.end()) {
                        Shell::process_pipes.erase(client.getIndex()+1);
                    }

                    erase_lise[i] = 1;
                } else {
                    client.prompt();
                }
            }
        }
        int count = Client::clients.size();
        for (int i=0; i<count ;i++) {
            if (erase_lise[i]) {
                Client::indice[Client::clients[i].getIndex()] = 0;
                Client::clients.erase(Client::clients.begin() + i);
            }
        }
    }

    close(listen_fd);
    return 0;
}