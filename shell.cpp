#include "shell.h"
#include "unit.h"
#include "client.h"

#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>

int Shell::state_code = 1;
std::map<int, std::map<int, int> > Shell::process_pipes;
// ============================== Public ==============================
Shell::Shell() {}

Shell::~Shell() {}

// ============================== Private ==============================

std::vector<Command> Shell::parseCommand(std::string input, int sock_fd) {
    std::vector<Command> commands;

    std::string command_name("");
    std::string arguments("");

    bool has_read_command = false;
    std::string *temp = &command_name;

    for (int i=0; i<input.size(); i++) {
        char c = input[i];

        if ((i == input.size()-1) && command_name.size()) {
            Command cmd(command_name, arguments);
            cmd.entire_command = input.substr(0, input.size()-1);
            commands.push_back(cmd);
            break;
        }

        if (c == PIPE_SYMBOL) {
            has_read_command = false;
            temp = &command_name;

            Command cmd(command_name, arguments);
            cmd.entire_command = input.substr(0, input.size()-1);
            command_name.clear();
            arguments.clear();

            std::string next_word("");
            for (i=i+1;i<input.size()-1;i++) {
                c = input[i];
                if (c == ' ') {
                    break;
                } else {
                    next_word.append(1u, c);
                }
            }

            int pipe_number = 1;
            if (next_word.size()) {
                if (unit::isDigits(next_word)) {
                    pipe_number = atoi(next_word.c_str());
                } else {
                    command_name = next_word;
                    has_read_command = true;
                    temp = &arguments;
                }
            }

            cmd.setPipeNumber(pipe_number);

            if (cmd.getName().size()) {
                commands.push_back(cmd);
            }

        } else if (c == ' ') {
            if (has_read_command) {
                temp = &arguments;
                temp->append(1u, c);
            } else {
                temp = &command_name;
            }

        } else {
            has_read_command = true;
            temp->append(1u, c);

        }
    }

    return commands;
}

void Shell::execCommand(std::vector<Command> commands, int sock_fd) {
    for (int cmd_index=0; cmd_index < commands.size(); cmd_index++) {
        Command cmd = commands[cmd_index];

        passPipe();

        int cmd_type = cmd.getCommandType();

        int fd_stdin = sock_fd;
        int fd_stdout = sock_fd;

        int fd[2];

        std::vector<int> child_close_list;
        std::vector<int> parent_close_list;

        for (Pipe p : pending_pipes) {
            if (p.getCount() == 0) {
                fd_stdin = p.getFdRead();
            } else {
                child_close_list.push_back(p.getFdRead());
            }
        }

        if (cmd.getInFd() > 0) {
            int accepter_index = -1;
            std::string accepter;
            int teller_index = -1;
            std::string teller;

            for (int i=0; i<Client::clients.size(); i++) {
                if (Client::clients[i].getIndex()+1 == cmd.getInFd()) {
                    teller = Client::clients[i].getName();
                    teller_index = Client::clients[i].getIndex()+1;
                }

                if (Client::clients[i].getSockFd() == sock_fd) {
                    accepter = Client::clients[i].getName();
                    accepter_index = Client::clients[i].getIndex()+1;
                }
            }

            bool found = false;
            if (Shell::process_pipes.find(accepter_index) != Shell::process_pipes.end()) {
                std::map<int, int> mp = Shell::process_pipes.find(accepter_index)->second;
                if (mp.find(teller_index) != mp.end()) {
                    fd_stdin = mp.find(teller_index)->second;

                    std::string info = "*** " + accepter + " (#" + unit::to_string(accepter_index) +") just received from " +
                                      teller + " (#" + unit::to_string(teller_index) +
                                     ") by '" + cmd.entire_command + "' ***\n";

                    for (int i=0; i<Client::clients.size(); i++) {
                        write(Client::clients[i].getSockFd(), info.c_str(), info.size());
                    }

                    found = true;
                    mp.erase(teller_index);
                    Shell::process_pipes[accepter_index] = mp;
                }
            }

            if (!found) {
                std::string info = "*** Error: the pipe #" + unit::to_string(cmd.getInFd())
                                    + "->#" + unit::to_string(accepter_index) +" does not exist yet. ***\n";
                write(sock_fd, info.c_str(), info.size());
                continue;
            }
        }

        if (cmd.getPipeNumber()) {
            Pipe *temp_pipe = getPipe(cmd.getPipeNumber());
            if (temp_pipe != nullptr) {
                fd_stdout = temp_pipe->getFdWrite();
            } else {
                if (pipe(fd) == -1) {
                    std::cout << "Fail to create pipe." << std::endl;
                }

                Pipe out_pipe = Pipe(fd[0], fd[1], cmd.getPipeNumber());
                pending_pipes.push_back(out_pipe);
                child_close_list.push_back(fd[0]);
                fd_stdout = fd[1];
            }
        }

        if (cmd.getFilename().size()) {
            fd_stdout = open(cmd.getFilename().c_str(), O_WRONLY | O_CREAT | O_TRUNC | O_CLOEXEC, 000777);
        }

        int close_fd = -1;
        if (cmd.getOutFd() > 0) {
            bool found = false;
            int accepter_index = -1;
            int teller_index = -1;
            std::string accepter;
            std::string teller;
            for (int i=0; i<Client::clients.size(); i++) {
                if (Client::clients[i].getIndex()+1 == cmd.getOutFd()) {
                    found = true;
                    accepter = Client::clients[i].getName();
                    accepter_index = Client::clients[i].getIndex()+1;
                }

                if (Client::clients[i].getSockFd() == sock_fd) {
                    teller = Client::clients[i].getName();
                    teller_index = Client::clients[i].getIndex()+1;
                }
            }

            if (!found) {
                std::string info = "*** Error: user #" + unit::to_string(cmd.getOutFd()) + " does not exist yet. ***\n";
                write(sock_fd, info.c_str(), info.size());
                continue;
            } else {
                int fd_p[2];
                pipe(fd_p);

                bool al_exist = false;
                if (Shell::process_pipes.find(accepter_index) != Shell::process_pipes.end()) {
                    std::map<int, int> mp = Shell::process_pipes.find(accepter_index)->second;
                    if (mp.find(teller_index) != mp.end()) {
                        std::string info = "*** Error: the pipe #" + unit::to_string(teller_index) +
                                             "->#" + unit::to_string(accepter_index) + " already exists. ***\n";
                        write(sock_fd, info.c_str(), info.size());
                        al_exist = true;
                    } else {
                        (Shell::process_pipes.find(accepter_index)->second)[teller_index] = fd_p[0];
                    }
                } else {
                    std::map<int, int> mp;
                    mp[teller_index] = fd_p[0];
                    Shell::process_pipes[accepter_index] = mp;
                }

                fd_stdout = fd_p[1];
                close_fd = fd_p[1];
                if (!al_exist) {
                    std::string info = "*** " + teller + " (#" + unit::to_string(teller_index) +") just piped '" +
                                         cmd.entire_command +"' to " + accepter + " (#" + unit::to_string(cmd.getOutFd()) +
                                         ") ***\n";
                    for (int i=0; i<Client::clients.size(); i++) {
                        write(Client::clients[i].getSockFd(), info.c_str(), info.size());
                    }
                } else {
                    close(fd_p[0]);
                    close(fd_p[1]);
                }
            }
        }

        if (cmd_type == UNKNOW_COMMAND_TYPE) {
            cmd.exec(sock_fd);
            if (cmd_index != 0)
                backPipe();

            if (getPipe(0) != nullptr) {
                close(getPipe(0)->getFdWrite());
            }

            break;
        } else if (cmd_type == INTER_COMMAND_TYPE) {
            if (getPipe(0) != nullptr) {
                close(getPipe(0)->getFdWrite());
            }

            cmd.exec(sock_fd);
        } else {
            if (getPipe(0) != nullptr) {
                close(getPipe(0)->getFdWrite());
            }

            pid_t pid = fork();
            if (pid < 0) {
                std::cout << "Fail to fork." << std::endl;
                exit(1);
            } else if (pid == 0) {
                dup2(fd_stdin, STDIN_FILENO);
                dup2(fd_stdout, STDOUT_FILENO);
                dup2(sock_fd, STDERR_FILENO);

                for (int close_fd : child_close_list) {
                    close(close_fd);
                }

                cmd.exec(sock_fd);
                exit(1);
            } else {
                if (getPipe(0) != nullptr) {
                    close(getPipe(0)->getFdRead());
                }

                if (close_fd > 0) 
                    close(close_fd);

                int status_code = 0;
                waitpid(pid, &status_code, 0);

                if (WIFEXITED(status_code)) {
                    if (WEXITSTATUS(status_code) > 0) {
                        cmd_index = commands.size();
                    }
                }
            }
        }

        cleanPipe();
    }

}

Pipe* Shell::getPipe(int count) {
    for (int i=0; i<pending_pipes.size(); i++) {
        if (pending_pipes[i].getCount() == count) {
            return &pending_pipes[i];
        }
    }

    return nullptr;
}

void Shell::passPipe() {
    for (int i=0; i<pending_pipes.size(); i++) {
        pending_pipes[i].passCount();
    }
}

void Shell::backPipe() {
    for (int i=0; i<pending_pipes.size(); i++) {
        pending_pipes[i].addCount();
    }
}

void Shell::cleanPipe() {
    for (int i=0; i<pending_pipes.size(); i++) {
        if (pending_pipes[i].getCount() <= 0) {
            pending_pipes.erase(pending_pipes.begin()+i);
        }
    }
}