#ifndef RAS_SHELL_H_
#define RAS_SHELL_H_

#include "pipe.h"
#include "command.h"

#include <string>
#include <vector>
#include <map>

#define MAX_LINE_CHAR_NUMBER 10000

#define PIPE_SYMBOL '|'

class Shell {
public:
    Shell();
    ~Shell();
    static int state_code;
    static std::map<int, std::map<int, int> > process_pipes;

    std::vector<Command> parseCommand(std::string input, int sock_fd);
    void execCommand(std::vector<Command> commands, int sock_fd);
    Pipe* getPipe(int count);
    void passPipe();
    void backPipe();
    void cleanPipe();
private:
    std::vector<Pipe> pending_pipes;
};

#endif