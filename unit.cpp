#include "unit.h"
#include <sstream>

bool unit::isDigits(const std::string &str) {
    return str.find_first_not_of("0123456789") == std::string::npos;
}

std::string unit::deleteStringFrontSpace(std::string str) {
    int start_index = 0;
    for (int i=0; i<str.size(); i++) {
        if (str[i] != ' ') {
            start_index = i;
            break;
        }
    }

    return str.substr(start_index);
}

std::string unit::to_string(int value) {
    std::stringstream ss;
    ss << value;
    return ss.str();
}